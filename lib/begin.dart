import 'package:flutter/material.dart';

class Begin extends StatefulWidget {
  const Begin({Key? key, required this.data,}) : super(key: key);

  final String data;

  @override
  _BeginState createState() => _BeginState(data);
}


class _BeginState extends State<Begin> {

  _BeginState(this.data);

  final String data;
  
  String data2 = "";


  @override
  Widget build(BuildContext context) {
    data2 = data.substring(0,15);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('POC Testi'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                'Tässä widgetissä suoritetaan testi ja tulos lähetetään palvelimelle',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ),
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                'Parametrit:',
                style: TextStyle(fontSize: 18),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'data-vasara: $data',
                style: const TextStyle(fontSize: 18),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text('Suoritetaan testi'),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Icon(Icons.arrow_downward),
            ),
            Card( child: const SizedBox(
              width: 300,
              height: 100,
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Text('{\n"businesskey": "dataa"\n"testResult": "dataa"\n}'),
              ),
            ),),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text('Tuloksen lähetys palvelimelle'),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Icon(Icons.arrow_downward),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Icon(Icons.cloud_queue, size: 30,),
            ),
            Padding(
              padding: const EdgeInsets.all(32.0),
              child: ElevatedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: const Text('Takaisin'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
