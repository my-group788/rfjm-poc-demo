import 'dart:async';

import 'package:flutter/material.dart';
import 'dart:io';
import 'package:webview_flutter/webview_flutter.dart';

import 'begin.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'POC Testi',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'POC Testi'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late WebViewController _webviewController;

  String? _url = 'https://idp.jyu.fi/nidp/app/login';
  String? _data_vasara = '-';

  bool _fetchingData = false;

  Future _getId() async {
    _data_vasara = await _webviewController.runJavascriptReturningResult(
        "document.querySelector('#rfjm').dataset.vasara");

    setState(() {
      _data_vasara = _data_vasara;
      _fetchingData = false;
    });
    return _data_vasara;
  }

  @override
  void initState() {
    super.initState();
    // Enable virtual display.
    if (Platform.isAndroid) WebView.platform = AndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(widget.title),
        ),
        body: Container(
          color: Colors.grey[200],
          child: Column(
            children: [
              SizedBox(
                height: 300,
                child: WebView(
                  onWebViewCreated: (controller) {
                    _webviewController = controller;
                  },
                  javascriptMode: JavascriptMode.unrestricted,
                  initialUrl: 'http://users.jyu.fi/~jptalask/rfjm/',
                  onPageStarted: (url) {
                    setState(() {
                      _url = url;
                    });
                  },
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(top: 20, bottom: 20.0, left: 10, right: 10),
                child: Column(
                  children: const [
                    Icon(
                      Icons.arrow_upward_sharp,
                      color: Colors.green,
                      size: 30.0,
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 30.0),
                      child: Text('Web-sivu'),
                    ),
                    Text(
                      'Haetaan ylläolevasta "Tästä data mobiilisovelluseen" html-elementistä data-vasara attribuutti',
                      style: TextStyle(fontSize: 16),
                    ),
                  ],
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(bottom: 20.0, left: 10, right: 10),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Text(
                    'data-vasara: $_data_vasara',
                    style: const TextStyle(
                        color: Colors.white,
                        backgroundColor: Colors.blue,
                        fontSize: 20),
                  ),
                ),
              ),
              _fetchingData
                  ? const SizedBox(
                      height: 25,
                      width: 25,
                      child: CircularProgressIndicator(
                        strokeWidth: 5,
                      ),
                    )
                  : _data_vasara == '-'
                      ? ElevatedButton(
                          onPressed: () {
                            setState(() {
                              _fetchingData = true;
                            });
                            Timer(const Duration(milliseconds: 1000), () {
                              _getId();
                            });
                          },
                          child: const Text('Hae'))
                      : ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.green,
                            onPrimary: Colors.white,
                          ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    Begin(data: _data_vasara!),
                              ),
                            );
                          },
                          child: const Text('Jatka'))
            ],
          ),
        ),
      ),
    );
  }
}
